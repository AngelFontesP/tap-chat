﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.IO;
using System.Windows.Forms;

namespace Cliente
{
    public partial class Form1 : Form
    {
       
        static private TcpClient cliente = new TcpClient(); // nos permitira acceder a la informacion 
        static private string nomusuario = "unknown";
        static private NetworkStream Finf; //permite manejar la informacion 
        static private StreamWriter writer; //escrribir en la informacion 
        static private StreamReader reader; // leer la informacion 

        private delegate void DAddItem(String s);
        //mandara informacion entre procesos esto se refiere al intercambio de informacion 

        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            listBox1.Visible = true;
            textBox2.Visible = true;
            button2.Visible = true;
            pictureBox2.Visible = true;
            pictureBox3.Visible = true;
            label1.Visible = false;
            pictureBox1.Visible = false;
            textBox1.Visible = false;
            button1.Visible = false;
            nomusuario = textBox1.Text;
            Conectar();
        }
        void Conectar()
        {
            try
            {
                cliente.Connect("127.0.0.1", 8000); //conectar con la direccion ip de la maquina 
                if (cliente.Connected)
                {
                    Thread hilo = new Thread(Escucha);

                    Finf = cliente.GetStream();
                    writer = new StreamWriter(Finf);
                    reader = new StreamReader(Finf);
                    writer.WriteLine(nomusuario);
                    writer.Flush();
                    hilo.Start();
                }
                else
                {
                    MessageBox.Show("EL SERVIDOR NO SE ENCUENTRA DISPONIBLE","INFORMACION",MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Application.Exit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("EL SERVIDOR NO ESTA DISPONIBLE","INFORMACION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Exit();
            }
        }

        void Escucha() //Escchar la informacion que nos envia el cliente y el serrvidor 
        {
            while (cliente.Connected)
            {
                try
                {
                    this.Invoke(new DAddItem(AddItem), reader.ReadLine());// Aqui es donde se registrara dicha informacion 
                    //entre el cliente y el sevidor 

                }
                catch
                {
                    MessageBox.Show("NO SE PUEDO ESTABLECER CONEXION CON EL SERVIDOR","INFORMACION", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Application.Exit();
                }
            }
        }

        private void AddItem(String s)
        {
            listBox1.Items.Add(s);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            writer.WriteLine(textBox2.Text);
            writer.Flush();
            textBox2.Clear();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            if(WindowState==FormWindowState.Normal)
            {
                WindowState = FormWindowState.Minimized;
            }
            else
            {
                WindowState = FormWindowState.Normal;
            }
        }

    }
}
