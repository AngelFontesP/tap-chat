﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Servidor00
{
    class Servidor
    {
        private IPEndPoint ip = new IPEndPoint(IPAddress.Any, 8000); //configura  la direccion ip 
        private List<Connection> lconexion = new List<Connection>(); // se crea una lista que guardara la informacion de los objetos 
        private TcpListener cservidor; //Espera las conexiones de los clientes 
        private TcpClient cliente = new TcpClient();//crear la conexion entre los cleintes y el sevidor 
        Connection sconexion;


        public void Inicio()
        {

            Console.WriteLine("SERVIDOR DISPONIBLE");
            cservidor = new TcpListener(ip);
            cservidor.Start();

            while (true)  //cuando un cleinte  nuevo se conecte se establecera la conexion
            {
                cliente = cservidor.AcceptTcpClient();

                sconexion = new Connection();
                sconexion.Finf = cliente.GetStream();
                sconexion.reader = new StreamReader(sconexion.Finf);
                sconexion.writer = new StreamWriter(sconexion.Finf);
                sconexion.nomusuario = sconexion.reader.ReadLine();
                lconexion.Add(sconexion);
                Console.WriteLine(sconexion.nomusuario + " Se ha conectado");
                Thread hilo= new Thread(Escuchar_conexion);
                hilo.Start();
            }
        }
        private struct Connection
        {
            public NetworkStream Finf;
            public StreamWriter writer;
            public StreamReader reader;
            public string nomusuario;
        }

        public Servidor()
        {
            Inicio();
        }

        void Escuchar_conexion()
        {
            Connection nconexion = sconexion;

            do
            {
                try
                {
                string caracteresm = nconexion.reader.ReadLine(); //leer los caracteres que ah enviado el cliente 
                    Console.WriteLine(nconexion.nomusuario + ": " + caracteresm );
                    foreach (Connection con in lconexion)
                    {
                        try
                        {
                            con.writer.WriteLine(nconexion.nomusuario + ": " + caracteresm);
                            con.writer.Flush();
                        }
                        catch
                        {
                        }
                    }
                }
                catch
                {
                    lconexion.Remove(nconexion);
                    Console.WriteLine(sconexion.nomusuario + " se a desconectado.");
                    break;
                }
            } while (true);
        }
    }
}
